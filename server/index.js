/**
 * 服务端API入口文件
 * @file index.js
 * @author Verning Aulence
 * @date 2021-09-07 15:03:56
 */

// 域名和端口号配置
const baseURL = '127.0.0.1';
const port = 3001;
// express导入
const express = require('express');
const app  = express();
// 用户信息路由模块
const userInfoRouter = require('./route/user');
// 资源管理路由模块
const resourceRouter = require('./route/resource');
// 工具函数库
const {setConsole} = require('./modules/tools');

// 配置console加行号的输出方式
setConsole();

// 托管静态文件的目录设置
app.use('/assets/images', express.static('assets/images'));

// 默认首页
app.get('/', (req, res) => {
    res.send('首页')
});

// 获取用户信息的路由
app.use('/userinfo', userInfoRouter);

// 资源管理路由
app.use('/resource', resourceRouter);

app.listen(port, baseURL);
console.dir(`Ctrl+左键查看页面：http://${baseURL}:${port}`);
