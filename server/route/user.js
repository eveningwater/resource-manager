const express = require('express');
const router = express.Router();
const session = require('express-session');
// 自定义数据库模块
const {UserInfoModel} = require('../modules/db');
// MD5加密
const md5 = require('md5-node');

// 处理POST请求的中间件配置
router.use(express.json());
router.use(express.urlencoded({ extended: true }));

// 使用'express-session'中间件
router.use(session({
    // 签名模式
    secret: 'NE02m17d',
    // 设置session的名称，默认为“connect.sid”
    name: 'session_data',
    // 即使没有去设置过session也会有一个初始的值，默认值为true，建议值true
    saveUninitialized: false,
    // 基于session的数据对cookie进行同步的配置
    cookie: { maxAge: 1000 * 60 * 60 /* 60分钟过期 */ },
    // cookie: { maxAge: 1000 * 60 * 10 /* 10分钟过期 */ },
    // 只要当前页面触发过载入事件，那过期时间就会重置（值为true时）
    rolling: true,
    // 强制保存，即使是没有修改过的，建议取false值已节约不必要的资源开销
    resave: false
}));

// 用户信息获取
router.get('/', (req, res) => {
    res.send('用户信息');
});

// 用户登录
router.post('/user_login', (req, res) => {
    // 获取用户登录信息
    const userLoginInfo = req.body;
    
    // 如果如果没有获取到这个对象
    if(!userLoginInfo) {
        res.status(500).json({
            result: 'Fail',
            messeage: '没有获取到客户端发送的请求数据'
        });
        return;
    }
    UserInfoModel.findOne({
        username: userLoginInfo.username,
        userpassword: userLoginInfo.userpassword
    },(err, info) => {
        if(err) return console.error(err);
        // 没有查询到对应用户
        if(!info) {
            res.json({
                result: 'Fail',
                messeage: '用户名或密码错误'
            });
            return;
        }
        
        // 设置 session 信息
        req.session.userSession = info.username;

        // 向客户端返回成功结果
        res.json({
            result: 'Success',
            messeage: '用户登录成功',
            username: info.username,
            loginDate: new Date().toLocaleString(),
            session_info: md5(info.username)
        });
    });
});

// 验证登录状态
router.post('/validate_login', (req, res) => {
    // 如果符合
    if(req.body.sessionInfo === md5(req.session.userSession)) {
        res.send(true);
    }
    // 如果不符合
    else {
        res.send(false);
    }
});

// 退出登录
router.get('/login_out', (req, res) => {
    // 销毁所有 session 信息
    req.session.destroy(err => {
        if(err) return console.error(err);
        res.send('destroyed');
    });
});

// 注册用户
router.get('/user_register', (req, res) => {
    UserInfoModel.find((err, users) => {
        if(err) return console.error(err);

        res.send(users);
    })
    // const newUser = new UserInfoModel({
    //     username: 'test',
    //     userpassword: md5('1234'),
    //     userTel: '18628109681',
    //     avatar: '',
    //     gender: 1,
    //     birthDay: new Date('1990-11-22'),
    //     realName: '欧冷斯',
    //     idCardNum: '510524199011224321',
    //     regDate: new Date().toLocaleString()
    // });
    // newUser.save((err, info) => {
    //     if(err) return console.error(err);
    //     console.log(info);
    //     res.send('用户注册成功');
    // });
});

module.exports = router;