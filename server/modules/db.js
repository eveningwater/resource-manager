/**
 * Mongoose数据库操作模型定义
 * @file db.js
 * @author Verning Aulence
 * @date 2021-09-04 17:01:26
 */

const dbURL = 'mongodb://localhost:27017';
// 数据库引入和配置
const mongoose = require('mongoose');
// 连接指定的数据库
mongoose.connect(`${dbURL}/resource_manager`);
// 禁用那该死的集合(s)的复数形式
mongoose.pluralize(null);
// 创建数据库连接
const db = mongoose.connection;

// 监听数据库连接错误
db.on('error', console.error.bind(console, '连接错误：'));
// 开启数据库，并进行数据操作
db.once('open', () => {
    console.log('数据库连接成功！');
});
// 监听数据库关闭
db.once('close',()=>{
    console.log('数据库已断开！')
});
// 登录信息的数据模式配置
const userInfoSchema = mongoose.Schema({
    username: String,
    userpassword: String,
    userTel: String,
    avatar: String,
    gender: Number,
    birthDay: Date,
    realName: String,
    idCardNum: String,
    regDate: Date
});
// 登录信息的数据模型
const UserInfoModel = mongoose.model('userinfo', userInfoSchema);

// 资源的数据模式配置
const ResourceSchema = mongoose.Schema({
    uploadFromUser: String,
    resoOriginName: String,
    resoType: String,
    resoSize: Number,
    imageNaturalWidth: Number,
    imageNaturalHeight: Number,
    classify: [String],
    fileName: String,
    description: String,
    fileSavePath: String,
    filePreviewSavePath: String,
    serverRootFileSavePath: String,
    lastModifiedDate: Date,
    uploadDate: Date
});

// 暴露定义的模块
module.exports.Mongoose = mongoose;
module.exports.UserInfoModel = UserInfoModel;
module.exports.ResourceSchema = ResourceSchema;