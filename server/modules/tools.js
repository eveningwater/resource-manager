/**
 * 服务器端工具模块
 * @file tools.js
 * @author Verning Aulence
 * @date 2021-09-07 15:07:14
 */

/**
 * 重定义console对象的输出方式为带行号的形式
 */
module.exports.setConsole = () => {
    ["log", "dir", "table", "warn", "error"].forEach((methodName) => {
        let cons = console[methodName];
        console[methodName] = (...args) => {
            try {
                throw new Error();
            } catch (err) {
                cons.apply(console, [
                    ...args,
                    "\t",
                    "（执行时间点：" + new Date().toLocaleTimeString() + "）",
                    "\n",
                    err.stack
                        .split("\n")[2]
                        .trim()
                        .substring(3)
                        .replace(__dirname, "")
                        .replace(/\s\(./, " at ")
                        .replace(/\)/, "")
                ]);
            }
        };
    });
}
