# Aulence资源管理库的开发计划

## 服务器端

- [x] 在图片上传的时候，对图片进行一份压缩另存
- [ ] 用户注册接口
- [x] 完成资源分类返回的接口
- [x] 完成资源文件的删除接口
- [x] 完成资源文件的修改接口
- [x] 实现在用户上传的资源存储在以自己账号为名的文件夹中
- [ ] 完成插件上传的接口

## 资源管理系统（PC端）

- [x] 完成左侧导航所有的分类页面路由
- [x] 完成资源展示页的小图预览（目前使用的是原图预览），以增加页面载入速度
- [x] 在资源查看页面顶部加入数量统计
- [ ] 用户注册的实现（需要激活码才能注册成功）
- [x] 完成图片资源分类的载入，而不是为所有分类都载入同样的图片
- [x] 资源文件在上传完成后，如果用户选择“前往查看”，左侧导航需要展开对应的栏目，且选中正确的分类
- [x] 在资源面板上显示资源的名称、大小和上传日期等信息
- [x] 完成资源面板上的“复制链接”功能
- [x] 完成资源面板上的“下载文件”功能
- [x] 完成资源面板上的“查看详情”功能
- [x] 在适当的位置加入资源的删除功能
- [x] 在适当的位置加入资源的修改功能
- [x] 完成资源面板上的“修改资源信息”功能
- [x] 完成资源面板上的“删除资源”功能
- [ ] 完成插件上传的完整流程

