import React, { useState, useEffect, axios } from 'react';
import { message } from 'antd';
import qs from 'query-string';
// 面包屑导航
import BreadNav from '../../../components/BreadNav/BreadNav';
// 资源列表面板组件
import ResourceListPanel from '../../../components/ResourceListPanel/ResourceListPanel';

/**
 * 【资料文档】组件页面
 */
function Documentation(props) {
    // 资源列表状态
    const [resourceList, setResourceList] = useState([]);

    /**
     * 生命周期：useEffect
     */
    useEffect(() => {
        // 获取分类名
        const {classifyName} = qs.parse(props.location.search);

        // 请求资源数据
        axios.get('/api/resource/get_all_picture', {
            params: {
                username: sessionStorage.getItem('username')
            }
        }).then(res => {
            // 查询出对应的分类数据
            const classifyData = res.data.filter(item => {
                return item.classify[1] === classifyName;
            });
            
            // 设置资源列表数组
            setResourceList(classifyData);
            
        }).catch(err => {
            message.error('资源载入发生错误：', err);
        });
    }, [props.location.search]);

    return (
        <div className="Smallicon">
            {/* 面包屑导航 */}
            <BreadNav locations={props.location}/>
            
            {/* 资源列表主面板 */}
            {
                resourceList.length ?
                <ResourceListPanel resourceList={resourceList} /> :
                <div className="not-resource">-- 当前分类暂无任何上传资源 --</div>
            }
        </div>
    )
}

export default Documentation;
