import React from 'react';
// 面包屑导航
import BreadNav from '../../../components/BreadNav/BreadNav';

function ClassifyStatistic(props) {
    return (
        <div className="ClassifyStatistic">
            {/* 面包屑导航 */}
            <BreadNav locations={props.location}/>
            分类统计
        </div>
    )
}

export default ClassifyStatistic
