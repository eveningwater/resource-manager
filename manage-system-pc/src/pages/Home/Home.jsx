import React from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
// 左侧导航组件
import LeftNav from '../../components/LefNav/LefNav';
// 统计和管理页面
import StatisticHome from './StatisticHome/StatisticsHome';
import ClassifyStatistic from './ClassifyStatistic/ClassifyStatistic';
import ResourceManage from './ResourceManage/ResourceManage';
// 图片资源页面
import Pictures from './Pictures/Pictures';
// 插件库页面
import Plugins from './Plugins/Plugins';
// 项目存档页面
import Projects from './Projects/Projects';
// 资料文档页面
import Documentation from './Documentation/Documentation';
// 视频短片页面
import Videos from './Videos/Videos';


function Home() {
    return (
        <div className="Home">
            {/* 左侧导航 */}
            <LeftNav />

            {/* 右侧内容视图 */}
            <Switch>
                {/* 统计和管理 */}
                <Route path="/home/statistichome" component={StatisticHome} />
                <Route path="/home/classifystatistic" component={ClassifyStatistic} />
                <Route path="/home/resourcemanage" component={ResourceManage} />
                {/* 图片资源 */}
                <Route path="/home/pictures" component={Pictures} />
                {/* 插件库 */}
                <Route path="/home/plugins" component={Plugins} />
                {/* 项目存档 */}
                <Route path="/home/projects" component={Projects} />
                {/* 资料文档 */}
                <Route path="/home/documentation" component={Documentation} />
                {/* 视频短片 */}
                <Route path="/home/videos" component={Videos} />
                {/* 默认到统计首页 */}
                <Redirect to="/home/statistichome" />
            </Switch>
        </div>
    )
}

export default Home;
