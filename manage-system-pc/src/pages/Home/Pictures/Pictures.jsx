import React, { useState, useEffect, axios } from 'react';
import { message } from 'antd';
import qs from 'query-string';
// 面包屑导航
import BreadNav from '../../../components/BreadNav/BreadNav';
// 资源列表面板组件
import ResourceListPanel from '../../../components/ResourceListPanel/ResourceListPanel';

function Pictures(props) {
    // 资源列表状态
    const [resourceList, setResourceList] = useState([]);

    // 视觉上删除某个资源
    const removeResourceItem = (index) => {
        // 克隆出一个新的资源列表数组
        // 不这样做数组的内存地址一致，不会触发视图刷新
        const cloneResourceList = [...resourceList];

        // 删除指定索引的资源列表项
        cloneResourceList.splice(index, 1);
        
        // 刷新视图上的资源列表
        setResourceList(cloneResourceList);
    }

    /**
     * 生命周期
     */
    useEffect(() => {
        // 获取分类名
        const {classifyName} = qs.parse(props.location.search);

        // 请求图片数据资源
        axios.get('/api/resource/get_all_picture', {
            params: {
                username: sessionStorage.getItem('username')
            }
        }).then(res => {
            // 查询出对应的分类数据
            const classifyData = res.data.filter(item => {
                return item.classify[1] === classifyName;
            });
            
            // 设置资源列表数组
            setResourceList(classifyData);
            
        }).catch(err => {
            message.error('资源载入发生错误：', err);
        });
    }, [props.location.search]);

    return (
        <div className="Pictures">
            {/* 面包屑导航 */}
            <BreadNav locations={props.location}/>

            {/* 资源列表主面板 */}
            {
                resourceList.length ?
                <ResourceListPanel 
                    resourceList={resourceList} 
                    routeHistoryTo={props.history.push} 
                    removeItem={removeResourceItem}
                /> :
                <div className="not-resource">-- 当前分类暂无任何上传资源 --</div>
            }
            
        </div>
    )
}

export default Pictures
