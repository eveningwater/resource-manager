import React, {useEffect,axios} from 'react';
import './StatisticHome.less';
// 面包屑导航
import BreadNav from '../../../components/BreadNav/BreadNav';
// 本页面对应的功能
import {allStatisticOptionsPie, allStatisticOptionsBar} from './statistics-home';
// ECharts 图表相关
import * as echarts from 'echarts/core';
import {
    PieChart,
    BarChart
} from 'echarts/charts';
import {
    TitleComponent,
    TooltipComponent,
    GridComponent
} from 'echarts/components';
import {
    SVGRenderer
} from 'echarts/renderers';

// 注册ECharts组件
echarts.use([
    TitleComponent, 
    TooltipComponent, 
    GridComponent, 
    PieChart, 
    BarChart,
    SVGRenderer
]);

// 统计首页
function StatisticHome(props) {
    // 生命周期处理
    useEffect(() => {
        // 全部分类统计图表（饼状图）
        let allStatisticPieChart = null;
        
        // 防止图标已经绘制后再次绘制的警告
        if(allStatisticPieChart !== null) {
            console.log('ECharts',allStatisticPieChart);
            allStatisticPieChart.dispose();
        }
        // 创建全部分类统计图表实例（饼状图） 
        allStatisticPieChart = echarts.init(
            document.getElementById('allStatisticPie'),
            null,
            {renderer: 'svg'}
        );
        
        // 全部分类统计图表（饼状图）
        let allStatisticBarChart = null;
        
        if(allStatisticBarChart !== null) {
            console.log('ECharts',allStatisticBarChart);
            allStatisticBarChart.dispose();
        }

        // 创建全部分类统计图表实例（柱状图）
        allStatisticBarChart = echarts.init(
            document.getElementById('allStatisticBar'),
            null,
            {renderer: 'svg'}
        );

        // 请求到资源分类数据
        axios.get('/options/allResourceStatistic.json').then(res => {
            // 绘制全部分类统计图表（饼状图）
            allStatisticPieChart.setOption(allStatisticOptionsPie(res.data));
            // 绘制全部分类统计图表（柱状图）
            allStatisticBarChart.setOption(allStatisticOptionsBar(res.data));
        });

        // 浏览器窗口缩放事件
        window.onresize = () => {
            // 调整图表的大小以适应浏览器
            allStatisticPieChart.resize();
            allStatisticBarChart.resize();
        }
    },[]);

    return (
        <div className="StatisticHome">
            {/* 面包屑导航 */}
            <BreadNav locations={props.location}/>

            {/* 统计图表部分 */}
            <div className="statistic-charts">
                {/* 全部分类统计图表（饼状图） */}
                <div id="allStatisticPie" className="all-statistic-pie"></div>
                {/* 全部分类统计图表（柱状图） */}
                <div id="allStatisticBar" className="all-statistic-bar"></div>
            </div>
        </div>
    )
}

export default StatisticHome
