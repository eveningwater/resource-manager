/**
 * 统计首页的功能
 * @file statistics-home.js
 * @author Verning Aulence
 * @date 2021-09-09 15:50:18
 */

/**
 * 全部分类统计图表配置项（饼状图）
 * @param {Array} seriesData 系列数据
 * @returns 全部分类统计的图标配置对象
 */
export const allStatisticOptionsPie = (seriesData) => {
    // 返回图标配置对象
    return {
        title: {
            text: '分类所占比例统计',
            left: 'center',
            top: '20px',
            textStyle: {
                color: '#4b2a13'
            }
        },
        tooltip:{},
        series: [
            {
                name: '资源占有数',
                type: 'pie',
                radius: ['10%', '65%'],
                center: ['50%', '55%'],
                roseType: 'area',
                itemStyle: {
                    borderRadius: 8
                },
                data: seriesData
            }
        ]
    }
}

/**
 * 全部分类统计图表配置项柱状图
 * @param {Array} seriesData 系列数据
 * @returns 全部分类统计的图标配置对象
 */
export const allStatisticOptionsBar = (seriesData) => {
    // 自定义柱状图颜色集合
    const barColors = ['#5470c6', '#91cc75', '#fac858', '#ee6666', '#73c0de', '#3ba272', '#fc8452', '#9a60b4', '#ea7ccc'];
    
    // 根据传入的参数生成serise.data的数组
    const setSeriseData = seriesData.map((classify,index) => {
        return {
            value: classify.value,
            itemStyle: {color: barColors[index]}
        }
    });

    // 返回图标配置对象
    return {
        title: {
            text: '分类总数量统计',
            left: 'center',
            top: '20px',
            textStyle: {
                color: '#4b2a13'
            }
        },
        tooltip: {},
        grid: {
            left: '60px',
            top: '66px',
            right: '20px',
            bottom: '36px'
        },
        xAxis: {
            type: 'category',
            data: seriesData.map(classify => classify.name)
        },
        yAxis: {
            type: 'value'
        },
        series: [{
            type: 'bar',
            data: setSeriseData
        }]
    }
}

