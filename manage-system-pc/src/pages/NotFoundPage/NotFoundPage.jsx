import React from 'react';
import './NotFoundPage.less';
import notFoundImg from '../../assets/images/404.png'

function NotFoundPage() {
    return (
        <div className="NotFoundPage">
            <div className="not-found-img">
                <img src={notFoundImg} alt="找不到页面" />
            </div>
        </div>
    )
}

export default NotFoundPage
