import React from "react";
import ReactDOM from "react-dom";
import {BrowserRouter} from 'react-router-dom'
import axios from 'axios';
import "./index.less";
import App from "./App";

// 配置axios的一些默认属性
// 超时时间为5秒
axios.defaults.timeout = 5000;

// 将 axios 添加成 React 的核心模块
React.axios = axios;


ReactDOM.render(
    <BrowserRouter>
        <App />
    </BrowserRouter>,
    document.getElementById("root")
);