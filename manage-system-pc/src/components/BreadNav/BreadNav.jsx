import React from 'react';
import './BreadNav.less';
import { Breadcrumb } from 'antd';
import {
    EnvironmentOutlined
} from "@ant-design/icons";
import qs from 'query-string';

// 导航映射表
const breadcrumbMap = {
    "home": "管理首页",
    "statistichome": "统计和管理",
    "classifystatistic": "统计和管理",
    "resourcemanage": "统计和管理",
    "pictures": "图片资源",
    "smallicon": "小图标",
    "plugins": "插件库",
    "projects": "项目存档",
    "documentation": "资料文档",
    "videos": "视频短片"
}

/**
 * 面包屑动态导航组件
 * @param {Object} props 包含了路由 location 对象的属性对象
 */
function BreadNav(props) {
    // 获取这个location 对象
    const locations = props.locations;
    // 获取资源分类名
    const {classifyName} = qs.parse(locations.search);
    // 将PathName分割为数组
    const pathnameArr = locations.pathname
        .split('/')
        // 返回不为空的数组项
        .filter(nameItem => nameItem !== '' );

    // 设置1/2/3级路由的文本名称
    const navLev1 = breadcrumbMap[pathnameArr[0]];
    const navLev2 = breadcrumbMap[pathnameArr[1]];
    let navLev3;
    switch (pathnameArr[1]) {
        case 'statistichome':
            navLev3 = '统计首页';
            break;
        case 'classifystatistic':
            navLev3 = '分类统计';
            break;
        case 'resourcemanage':
            navLev3 = '资源管理';
            break;
        case undefined:
            navLev3 = null;
            break;
        default:
            navLev3 = classifyName;
    }

    return (
        <Breadcrumb separator=">">
            <Breadcrumb.Item>
                <EnvironmentOutlined className="location" />
                {navLev1}
            </Breadcrumb.Item>
            <Breadcrumb.Item>{navLev2}</Breadcrumb.Item>
            {navLev3 ? <Breadcrumb.Item>{navLev3}</Breadcrumb.Item> : ''}
        </Breadcrumb>
    )
}

export default BreadNav
