import React,{ axios } from 'react';
import { Divider, Image, ConfigProvider, Popover, notification, Modal } from 'antd';
import { LinkOutlined, CloudDownloadOutlined, FileSearchOutlined, EditOutlined, DeleteOutlined, ExclamationCircleOutlined } from '@ant-design/icons';
import zh_CN from 'antd/lib/locale-provider/zh_CN';
import './ResourceListPanel.less';
// 图片加载失败时使用的地址
import { defaultImage } from '../../assets/tools/base64-image';
// 订阅发布模块
import PubSub from 'pubsub-js';
// 确认模态框
const { confirm } = Modal;

/**
 * 复制的气泡框
 */
const popoverCopyContent = (
    <div className="custom-popover copy-resource-link">
        <p className="copy-preview-resource-link">预览图</p>
        <Divider type="vertical" className="opretation-divider" />
        <p className="copy-origin-resource-link">原图</p>
    </div>
  );

/**
 * 资源列表面板组件
 */
function ResourceListPanel(props) {
    /**
     * 复制链接事件
     * @param {Number} index 当前对应资源数组的索引
     * @returns {Function} 点击事件函数
     */
    const copyLinkEvent = index => {
        // 点击复制事件
        return e => {
            // 获取元素的 ClassName
            const targetClassName = e.target.className;
            // 如果为复制预览图的链接按钮
            if(targetClassName === 'copy-preview-resource-link') {
                // 将预览文件路径添加到系统的剪切板
                navigator.clipboard.writeText(props.resourceList[index].filePreviewSavePath || props.resourceList[index].fileSavePath)
                    .then(() => {
                        // 成功提示
                        notification.success({
                            message: '链接复制成功！',
                            description: '您现在可以将链接粘贴到需要的地方进行访问',
                            duration: 1.8
                        })
                    });
            }
            // 如果为复制原图的链接按钮
            else if(targetClassName === 'copy-origin-resource-link') {
                // 将原文件路径添加到系统的剪切板
                navigator.clipboard.writeText(props.resourceList[index].fileSavePath)
                .then(() => {
                    // 成功提示
                    notification.success({
                        message: '链接复制成功！',
                        description: '您现在可以将链接粘贴到需要的地方进行访问',
                        duration: 1.8
                    })
                });
            }
        }
    }

    /**
     * 下载资源文件事件
     * @param {Number} index 当前对应资源数组的索引
     * @returns {Function} 点击事件函数
     */
    const downloadResourceFile = index => {
        return () => {
            // 获取文件在服务器的路径
            const resoURL = props.resourceList[index].serverRootFileSavePath;
            // 请求并下载这个资源的原文件
            axios.get(`/api/${resoURL}`, {
                // 使用这种响应方式来防止图片是预览而不是下载
                responseType: "blob"
            }).then(res => {
                // 创建虚拟的超链接及对应的属性
                const downloadLink = document.createElement('a');
                // 创建一个对象型 URL 而不是文件的 HTTP(s) 路径
                downloadLink.href = URL.createObjectURL(res.data);
                // 设置下载文件的名称为原文件名
                downloadLink.download = props.resourceList[index].fileName;
                // 触发超链接的点击下载事件
                downloadLink.click();
                // 移除该链接，释放占用
                downloadLink.remove();
            });
        }
    }

    /**
     * 显示资源的详情模态框
     * @param {Number} index 当前对应资源数组的索引
     * @returns {Function} 点击事件函数
     */
    const showResourceDetails = index => {
        return () => {
            // 获取资源信息
            const resoInfo = props.resourceList[index];
            
            // 资源信息模态框
            Modal.info({
                title: '资源详情信息',
                content: (
                    <div className="confirm-info-panel">
                        <p>
                            <span className="label">资源名称</span>
                            <span className="text">{resoInfo.fileName}</span>
                        </p>
                        <p>
                            <span className="label">资源原名</span>
                            <span className="text">{resoInfo.resoOriginName}</span>
                        </p>
                        <p>
                            <span className="label">所属分类</span>
                            <span className="text">{resoInfo.classify.join(' - ')}</span>
                        </p>
                        <p>
                            <span className="label">传输体积</span>
                            <span className="text">
                                {resoInfo.resoSize < 1024000 ?
                                    (resoInfo.resoSize / 1024).toFixed(2) + ' KB' :
                                    (resoInfo.resoSize / 1024 / 1024).toFixed(2) + ' MB'
                                }
                            </span>
                        </p>
                        <p>
                            <span className="label">实际宽高</span>
                            <span className="text">
                                {resoInfo.imageNaturalWidth} &times; {resoInfo.imageNaturalHeight}
                                &nbsp;像素
                            </span>
                        </p>
                        <p>
                            <span className="label">上传用户</span>
                            <span className="text">{resoInfo.uploadFromUser}</span>
                        </p>
                        <p>
                            <span className="label">资源创建日期</span>
                            <span className="text">{new Date(resoInfo.lastModifiedDate).toLocaleString()}</span>
                        </p>
                        <p>
                            <span className="label">上传日期</span>
                            <span className="text">{new Date(resoInfo.uploadDate).toLocaleString()}</span>
                        </p>
                        <p>
                            <span className="label">备注信息</span>
                            <span className="text">{resoInfo.description}</span>
                        </p>
                    </div>
                ),
                width: 'max-content'
            });
        }
    }

    /**
     * 删除当前资源的事件
     * @param {Number} index 当前对应资源数组的索引
     * @returns {Function} 点击事件函数
     */
    const deleteTheResource = index => {
        return () => {
            // 获取资源信息
            const resoInfo = props.resourceList[index];

            // 确认操作提示框
            confirm({
                title: `您确定要删除【${resoInfo.fileName}】这个资源吗?`,
                icon: <ExclamationCircleOutlined />,
                // content: '谢谢您对本站的支持，希望您再下次光临 ^_^',
                okText: '删掉',
                okType: 'danger',
                cancelText: '算了',
                onOk() {
                    // 获取用户名
                    const username = sessionStorage.getItem('username');

                    // 提交向服务器删除的请求
                    axios.delete(`/api/resource/delete/${username}/${resoInfo._id}`).then(res => {
                        // 删除成功
                        if(res.data.result === 'success') {
                            // 执行父组件上的视觉移除列表项方法
                            props.removeItem(index);

                            // 成功提示
                            notification.success({
                                message: '资源删除成功！',
                                duration: 1.8
                            });
                        }
                        else {
                            // 异常提示
                            notification.error({
                                message: '出现未知异常！',
                                description: res.data.reason,
                                duration: 1.8
                            });
                        }
                    }).catch(err => {
                        console.error('删除发生错误：', err);
                    });
                }
            });
            
        }
    }

    return (
        <ConfigProvider locale={zh_CN}>
            {/* 资源列表面板 */}
            <div className="ResourceListPanel">
                {/* 资源数量统计 */}
                <div className="resource-counter">
                    当前分类有 {props.resourceList.length} 个资源
                </div>

                {/* 资源列表 */}
                <div className="resource-list">
                    {props.resourceList.map((reso, index) => {
                        return (<div className="resource-item" key={reso._id}>
                            {/* 图片容器 */}
                            <figure className="image-panel">
                                <Image 
                                    src={reso.filePreviewSavePath || reso.fileSavePath} 
                                    alt="资源图片"
                                    title="点击预览大图"
                                    fallback={defaultImage}
                                    style={
                                        reso.imageNaturalWidth <= 512 ? 
                                        {
                                            width: 'auto',
                                            height: 'auto',
                                            objectFit: 'contain'
                                        } : ''
                                    }
                                />
                                <figcaption>
                                    <div>
                                        <span title={reso.fileName}>
                                            {reso.fileName}
                                        </span>
                                        <span title="图片体积">
                                            （{reso.resoSize < 1024000 ?
                                                (reso.resoSize / 1024).toFixed(2) + ' kb' :
                                                (reso.resoSize / 1024 / 1024).toFixed(2) + ' mb'
                                            }）
                                        </span>
                                    </div>
                                    <div title="上传日期">
                                        {new Date(reso.uploadDate).toLocaleString()}
                                    </div>
                                </figcaption>
                            </figure>
                            {/* 操作栏 */}
                            <div className="operation-bar">
                                <div 
                                    className="operation-bar-btn copy-link" 
                                    title="复制链接"
                                    onClick={copyLinkEvent(index)}
                                >
                                    <Popover content={popoverCopyContent} title="选择链接类型" trigger="hover">
                                        <LinkOutlined />
                                        链接
                                    </Popover>
                                </div>
                                <Divider type="vertical" className="opretation-divider" />
                                <div 
                                    className="operation-bar-btn download-file" 
                                    title="下载文件"
                                    onClick={downloadResourceFile(index)}
                                >
                                    <CloudDownloadOutlined />
                                    下载
                                </div>
                                <Divider type="vertical" className="opretation-divider" />
                                <div 
                                    className="operation-bar-btn show-details" 
                                    title="查看详情"
                                    onClick={showResourceDetails(index)}
                                >
                                    <FileSearchOutlined />
                                    详情
                                </div>
                                <Divider type="vertical" className="opretation-divider" />
                                <div 
                                    className="operation-bar-btn edit-resource" 
                                    title="修改资源信息"
                                    onClick={() => {
                                        // 发布一个导航展开事件（'statistichome'为第一个导航菜单）
                                        PubSub.publish('setOpenMenu', 'statistichome');
                                        // 跳转到该页面组件，并传递一个当前资源信息的state过去
                                        props.routeHistoryTo(`/home/resourcemanage`, reso);
                                    }}
                                >
                                    <EditOutlined />
                                    修改
                                </div>
                                <Divider type="vertical" className="opretation-divider" />
                                <div 
                                    className="operation-bar-btn delete-resource"
                                    title="删除资源"
                                    onClick={deleteTheResource(index)}
                                >
                                    <DeleteOutlined />
                                    删除
                                </div>
                            </div>
                        </div>);
                    })}
                </div>
            </div>
        </ConfigProvider>
    )
}

export default ResourceListPanel
