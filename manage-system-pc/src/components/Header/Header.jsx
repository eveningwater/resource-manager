import React,{useState} from 'react';
import './Header.less';
import defaultUserAvatar from '../../assets/images/default-user-avatar.svg';
import {Link, NavLink} from 'react-router-dom';

function Header(props) {
    // 获取用户名
    const sessionUserName = sessionStorage.getItem('username');
    const [userName] = useState(sessionUserName);

    

    return (
        <header className="Header">
            <div className="logo-nav">
                {/* 网站 Logo */}
                <div className="logo"></div>

                {/* 顶部导航 */}
                <nav className="top-nav">
                    <ul>
                        <li>
                            <NavLink to="/home">管理首页</NavLink>
                        </li>
                        <li>
                            <NavLink to="/usercenter">个人中心</NavLink>
                        </li>
                        <li>
                            <NavLink to="/aboutus">关于我们</NavLink>
                        </li>
                        <li>
                            <NavLink to="/aboutwebsite">关于网站</NavLink>
                        </li>
                    </ul>
                </nav>
            </div>
            

            {/* 用户信息和退出登录 */}
            <div className="user-profile">
                {/* 用户信息 */}
                <div className="user-info">
                    <Link to="/usercenter">
                        <div className="user-avatar">
                            <img src={defaultUserAvatar} alt="用户头像" />
                        </div>
                        欢迎您，<span>{userName}</span>
                    </Link>
                </div>
                {/* 退出登录 */}
                <div className="login-out">
                    <span onClick={props.userLoginOut}>退出登录</span>
                </div>
            </div>
        </header>
    )
}

export default Header
