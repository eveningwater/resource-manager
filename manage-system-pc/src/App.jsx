import React,{useState,lazy,Suspense,axios} from 'react';
import {Route,Redirect,Switch} from 'react-router-dom';
import "./App.less";
// Antd 组件库
import {Modal} from 'antd';
import {ExclamationCircleOutlined} from '@ant-design/icons';
// 载入等待组件页面
import Loading from './components/Loading/Loading';
// 页头组件
import Header from './components/Header/Header';
// 找不到页面
// import NotFoundPage from './pages/NotFoundPage/NotFoundPage';
// 管理统计首页页面
const Home = lazy(() => import('./pages/Home/Home'));
const UserCenter = lazy(() => import('./pages/UserCenter/UserCenter'));
const AboutUs = lazy(() => import('./pages/AboutUs/AboutUs'));
const AboutWebsite = lazy(() => import('./pages/AboutWebsite/AboutWebsite'));
// 登录页面
const LoginRegister = lazy(() => import('./pages/LoginRegister/LoginRegister'));
// 确认模态框
const { confirm } = Modal;

/**
 * 项目入口页面组件
 */
function App() {
    // 登录的状态
    const [userLogined, setUserLogined] = useState(false);

    /**
     * 设置登录状态的函数
     */
    const loginStateProcess = () => {
        // 获取会话中的用户信息
        const sessionInfo = sessionStorage.getItem('sessionInfo');
        const sessionUserName = sessionStorage.getItem('username');

        // 验证登录状态
        axios.post('/api/userinfo/validate_login', {
            sessionInfo
        }).then(res => {
            // 用户是否已登录
            const isLogined = res.data;
            // 如果已经登录，则设置自定义的用户名请求头
            if(isLogined) {
                // 设置用户名默认请求头
                axios.defaults.headers.common['username'] = sessionUserName;
            }
            // 设置登录状态
            setUserLogined(isLogined);
        })
    }

    /**
     * 显示退出登录的选择提示框
     */
    const showLoginOutConfirm = () => {
        confirm({
            title: '您确定要退出当前账号吗?',
            icon: <ExclamationCircleOutlined />,
            // content: '谢谢您对本站的支持，希望您再下次光临 ^_^',
            okText: '退出',
            okType: 'danger',
            cancelText: '算了',
            onOk() {
                // 移除session
                sessionStorage.removeItem('sessionInfo')
                // 设置用户为退出状态
                setUserLogined(false);
            }
        });
    }

    /**
     * 生命周期
     */
    React.useEffect(() => {
        // 设置登录状态的函数
        loginStateProcess();

        // 页面载入时发生
        console.log('%c运行时间：' + new Date().toLocaleString(), 'color: #03f939');
        // 监听更新的数据从而重新渲染页面组件
    }, []);

    // 如果用户已登录
    if(userLogined) {
        // 渲染主页面
        return (
            <div className="App">
                <Suspense fallback={<Loading />}>
                {/* 页头 */}
                <Header userLoginOut={showLoginOutConfirm} />
                    {/* 页面主体部分 */}
                    <Switch>
                        <Route path="/home" component={Home}></Route>
                        <Route path="/usercenter" component={UserCenter}></Route>
                        <Route path="/aboutus" component={AboutUs}></Route>
                        <Route path="/aboutwebsite" component={AboutWebsite}></Route>
                        <Redirect to="/home"/>
                    </Switch>
                </Suspense>
            </div>
        );
    }
    // 如果未登录
    else {
        // 渲染登录注册页
        return (
            <Suspense fallback={<Loading />}>
                <LoginRegister setLoginState={loginStateProcess}/>
            </Suspense>
        );
    }
}

export default App;
