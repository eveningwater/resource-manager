/**
 * 
 * @file setupProxy.js
 * @author Verning Aulence
 * @date 2021-09-09 15:59:39
 */

// 脚手架自带的模块，只需直接导入即可
const proxy = require('http-proxy-middleware');

module.exports = function(app) {
    app.use(
        proxy('/api', {
            // 请求目标地址
            target: 'http://127.0.0.1:3001',
            // 控制服务器收到的请求头中Host字段的值（更适用于跨域）
            changeOrigin: true,
            // 还原代理标识（服务器端将把路径中多余的无效路径前缀名去掉）
            pathRewrite: {'^/api': ''}
        }),
        // 项目配置数据
        proxy('/options', {
            target: 'http://api.aulence.com/resource-manage',
            changeOrigin: true,
            pathRewrite: {'^/options': ''}
        })
    )
}

