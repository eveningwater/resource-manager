/**
 * Webpack 配置覆盖文件
 * @file craco.config.js
 * @author Verning Aulence
 * @date 2021-09-07 16:18:58
 */

const CracoLessPlugin = require("craco-less");

module.exports = {
    plugins: [
        {
            plugin: CracoLessPlugin,
            options: {
                lessLoaderOptions: {
                    lessOptions: {
                        modifyVars: { "@primary-color": "#4b2a13" },
                        javascriptEnabled: true,
                    },
                },
            },
        },
    ],
};
